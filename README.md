##Setting up environment for docker (manual)

#update the apt package index
```
$ sudo apt-get update
```
#to allow apt to use a repository over HTTPS
```
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
```
#add the docker gpg key
```
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

#verify it by
```
$ sudo apt-key fingerprint 0EBFCD88
```

#setup repository
```
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```

##Installing docker

#Update the apt package index.
```
$ sudo apt-get update
```

#install the latest version by
```
$ sudo apt-get install docker-ce=<VERSION>
```

#verify it by running a container
```
$ sudo docker run hello-world
```

##installing in automatic way using script(make some changes)

```
$ curl -fsSL get.docker.com -o get-docker.sh
$ sudo sh get-docker.sh
```

##Setting up environment inside docker to run (not production)

#create directory
```
$ mkdir composetest
$ cd composetest
```

#create a package.json file
```
$echo "{
  "name": "docker_web_app",
  "version": "1.0.0",
  "description": "Node.js on Docker",
  "author": "Shubham Singh",
  "main": "server.js",
  "scripts": {
    "start": "node server.js"
  },
  "dependencies": {
    "express": "^4.16.1"
  }
}">package.json
```

#create a file called app.js

```
$echo "const express = require('express');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
  res.send('Hello world\n');
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);"> app.js

```
#create a docker file
```
$touch Dockerfile
```

#right this line inside it
```
$echo "
FROM node:5

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .   //to copy the remaining file inside our directory to docker

EXPOSE 8080

CMD [ "npm", "start" ]

">Dockerfile
```

#create a docker  ignore file
```
$echo "node_modules
npm-debug.log">.dockerignore
```

#build your docker image
```
$ docker build -t ssingh/node-web-app .
```

#to check our image
```
$docker images
```

#run the image
```
$ docker run -p 49160:8080 -d ssingh/node-web-app
```

#get the container id
```
$docker ps
```